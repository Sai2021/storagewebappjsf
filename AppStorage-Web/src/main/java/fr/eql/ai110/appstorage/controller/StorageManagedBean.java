package fr.eql.ai110.appstorage.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import fr.eql.ai110.appstorage.entity.StorageHouse;
import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.StorageHouseIBusiness;

@ManagedBean(name="mbStorage")
@SessionScoped
public class StorageManagedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbLogin.user}")
	private User connectedUser;
	
	private Set<StorageHouse> connectedUserStorages;	
	private List<StorageHouse> connectedStorages;	
	private String name;
	private Date createdate;
	
	@EJB
	private StorageHouseIBusiness storageHouseBusiness;
	
	private StorageHouse connectedStorage;	
	
	@PostConstruct
	public void init() {
		
		createdate = new Date();
		connectedStorage = new StorageHouse();
		connectedStorages = new ArrayList<StorageHouse>();
				
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		connectedUser = (User) session.getAttribute("connectedUser");
	}

	/**
	 * Operations 
	 * @return
	 */
	public String insert() {
		String forward = null;
		add();
		forward = "/storageView.xhtml?faces-redirection=true";
		return forward;		
	}	

	public String change() {
		String forward = null;
		update();
        forward = "/storageView.xhtml?faces-redirection=true";
        return forward;
	}
	
	public String clearUp() {
		String forward = null;
		delete();
		if (isDeleted()) {
			forward = "/storageView.xhtml?faces-redirection=true";
		}
		return forward;
	}
	
	public boolean isDeleted() {
		return connectedStorage == null;
	}
	
	public StorageHouse add() {
		return storageHouseBusiness.add(connectedStorage);
	}		
	public StorageHouse update() {
		return storageHouseBusiness.update(connectedStorage);
	}
	 public boolean delete() {
		return storageHouseBusiness.delete(connectedStorage);
	 }
	public StorageHouse getById(int id) {
		return storageHouseBusiness.getById(connectedStorage.getId());
	}			 	
	public List<StorageHouse> getAll() {
		connectedStorages = storageHouseBusiness.getAll();
		return connectedStorages;
	}	
	public String search(String name) {
		String forward = null;
		getByName(name) ;
	    forward = "/storageList.xhtml?faces-redirection=true";		
		return forward;		
	}	
	public StorageHouse getByName(String name) {		
		return storageHouseBusiness.getByName(name);
	}
	/**
	 * Entities getter and setter
	 * @return
	 */
	public StorageHouse getConnectedStorage() {
		return connectedStorage;
	}
	public void setConnectedStorage(StorageHouse connectedStorage) {
		this.connectedStorage = connectedStorage;
	}

	public StorageHouseIBusiness getStorageHouseBusiness() {
		return storageHouseBusiness;
	}

	public void setStorageHouseBusiness(StorageHouseIBusiness storageHouseBusiness) {
		this.storageHouseBusiness = storageHouseBusiness;
	}

	public User getConnectedUser() {
		return connectedUser;
	}

	public void setConnectedUser(User connectedUser) {
		this.connectedUser = connectedUser;
	}

	public Set<StorageHouse> getConnectedUserStorages() {
		return connectedUserStorages;
	}

	public void setConnectedUserStorages(Set<StorageHouse> connectedUserStorages) {
		this.connectedUserStorages = connectedUserStorages;
	}

	public List<StorageHouse> getConnectedStorages() {
		connectedStorages = storageHouseBusiness.getAll();
		return connectedStorages;
	}

	public void setConnectedStorages(List<StorageHouse> connectedStorages) {
		this.connectedStorages = connectedStorages;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
}
