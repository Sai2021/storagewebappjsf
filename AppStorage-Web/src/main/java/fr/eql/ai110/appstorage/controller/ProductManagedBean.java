package fr.eql.ai110.appstorage.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.appstorage.entity.Product;
import fr.eql.ai110.appstorage.entity.StorageHouse;
import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.ProductIBusiness;

@ManagedBean(name="mbProduct")
@SessionScoped
public class ProductManagedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManagedProperty(value="#{mbLogin.user}")
	private User connectedUser;
	
	@ManagedProperty(value="#{mbStorage.connectedStorage}")
	private StorageHouse storagehouse;
	
	private Set<Product> connectedUserProducts;
	
	@EJB
	private ProductIBusiness productBusiness;
	
	private Product connectedProduct;
	private String name;
	
	/**
	 * Product Maded in 
	 */
	private List<String> countries = new ArrayList<String>();
	private List<String> cities = new ArrayList<String>();
	private String selectedCountry;
	private String selectedCity;
	private Map<String, List<String>> citiesByCountry = new HashMap<String, List<String>>();
	
	
	
	@PostConstruct
	public void init() {
		connectedProduct = new Product();
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
		connectedUser = (User) session.getAttribute("connectedUser");
		connectedUserProducts = productBusiness.getProductByUser(connectedUser);
		
		Collections.addAll(countries, "France", "Germany", "USA", "Romania", "India", "China", "Japan");
		
		List<String> franceCities = new ArrayList<String>();
		List<String> germanyCities = new ArrayList<String>();
		List<String> usaCities = new ArrayList<String>();
		List<String> romaniaCities = new ArrayList<String>();
		List<String> indiaCities = new ArrayList<String>();
		List<String> chinaCities = new ArrayList<String>();
		List<String> japanCities = new ArrayList<String>();
		
		Collections.addAll(franceCities, "Paris", "Lyon", "Lille", "Nante", "Marseille");
		Collections.addAll(germanyCities, "Berlin", "Stuttgart", "Munich", "Hamburg", "Frankfurt");
		Collections.addAll(usaCities, "New York", "Seattle", "Los Angeles", "Chicago", "Houston");
		Collections.addAll(romaniaCities, "Bucharest", "Cluj Napoca", "Timisoara", "Lasi", "Craiova");
		Collections.addAll(indiaCities, "New Delhi", "Mumbai", "Chennai", "Bengaluru", "Kolkata");
		Collections.addAll(chinaCities, "Shenzhen", "Hangzhou", "Guangzhou", "Suzhou", "Shanghai");
		Collections.addAll(japanCities, "Tokyo", "Osaka", "Kyoto", "Hiroshima", "Seta");
		
		citiesByCountry.put("France", franceCities);
		citiesByCountry.put("Germany", germanyCities);
		citiesByCountry.put("USA", usaCities);
		citiesByCountry.put("Romania", romaniaCities);
		citiesByCountry.put("India", indiaCities);
		citiesByCountry.put("China", chinaCities);
		citiesByCountry.put("Japan", japanCities);
		
	}
	
	/***
	 * Operations
	 */
	public void onCountrySelected() {
		if (selectedCountry !=null && ! selectedCountry.equals("")) {
			cities= citiesByCountry.get(selectedCountry);
		} else {
			cities = new ArrayList<String>();
		}
	}	
	
	public String change() {
		String forward=null;
		update();
	    forward = "/productView.xhtml?faces-redirection=true";	
		return forward;
	}
	
	
	public String insert() {
		String forward=null;
		add();
	    forward = "/productView.xhtml?faces-redirection=true";	
		return forward;
	}
	
	public String clearUp() {
		String forward = null;
		delete();
		if (isDeleted()) {
			forward = "/productView.xhtml?faces-redirection=true";
		}
		return forward;
	}
	
	public boolean isDeleted() {
		return connectedProduct == null;
	}
	
	public Product update() {
		return productBusiness.update(connectedProduct);
	}	
	public void add() {
		connectedProduct = productBusiness.add(connectedProduct);
	}	
	public boolean delete() {
		return productBusiness.delete(connectedProduct);
	}	
	public Product getById(int id) {
		return productBusiness.getById(id);
	}	
	public List<Product> getAll(){
		return productBusiness.getAll();
	}
	public Set<Product> getProductByUser(User user){
		return productBusiness.getProductByUser(connectedUser);
	}
	public Product getByName(String name) {
		return productBusiness.getByName(name);
	}
	
	/***
	 * Setter and getter
	 */
	public User getConnectedUser() {
		return connectedUser;
	}
	public void setConnectedUser(User connectedUser) {
		this.connectedUser = connectedUser;
	}
	public Set<Product> getConnectedUserProducts() {
		return connectedUserProducts;
	}
	public void setConnectedUserProducts(Set<Product> connectedUserProducts) {
		this.connectedUserProducts = connectedUserProducts;
	}
	public ProductIBusiness getProductBusiness() {
		return productBusiness;
	}
	public void setProductBusiness(ProductIBusiness productBusiness) {
		this.productBusiness = productBusiness;
	}
	public Product getConnectedProduct() {
		return connectedProduct;
	}
	public void setConnectedProduct(Product connectedProduct) {
		this.connectedProduct = connectedProduct;
	}
	public StorageHouse getStoragehouse() {
		return storagehouse;
	}
	public void setStoragehouse(StorageHouse storagehouse) {
		this.storagehouse = storagehouse;
	}
	public List<String> getCountries() {
		return countries;
	}
	public void setCountries(List<String> countries) {
		this.countries = countries;
	}
	public List<String> getCities() {
		return cities;
	}
	public void setCities(List<String> cities) {
		this.cities = cities;
	}
	public String getSelectedCountry() {
		return selectedCountry;
	}
	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}
	public String getSelectedCity() {
		return selectedCity;
	}
	public void setSelectedCity(String selectedCity) {
		this.selectedCity = selectedCity;
	}
	public Map<String, List<String>> getCitiesByCountry() {
		return citiesByCountry;
	}
	public void setCitiesByCountry(Map<String, List<String>> citiesByCountry) {
		this.citiesByCountry = citiesByCountry;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	

}
