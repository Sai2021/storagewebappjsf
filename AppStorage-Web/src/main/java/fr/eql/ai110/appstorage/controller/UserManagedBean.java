package fr.eql.ai110.appstorage.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.AccountIBusiness;
import fr.eql.ai110.appstorage.ibusiness.UserIBusiness;

@ManagedBean(name="mbUser")
@SessionScoped
public class UserManagedBean implements Serializable{

	/**
	 * User DATA
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value="#{mbLogin.user}")
	private User connectedUser;	
	
	private String login;
	private String pwd;
	private String usertype;
	private List<String> uTypes = new ArrayList<String>(); //userType
	private String selectedUType;
	private Integer phone;
	private String memo;
	private String pageId;
		

	@PostConstruct
	public void init() {
		Collections.addAll(uTypes, "Client", "Partenaire", "Fournisseur");
	}

	@EJB
	private UserIBusiness userBusiness;
	@EJB
	private AccountIBusiness accountBusiness;
	
	// Test parts
	public String addPart() {
		String forward = null;
		connectedUser  = userBusiness.addPart(login, pwd, usertype, phone, memo);
		if (isAdded()) {
			forward = "/login.xhtml?faces-redirection=true";
		}else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"Identifiant et/ou mot de passe invalid(s)",
					"Votre infos invalid(s)"
					);
			FacesContext.getCurrentInstance().addMessage("createForm:inpLogin", facesMessage);		
			FacesContext.getCurrentInstance().addMessage("createForm:inpPwd", facesMessage);
			FacesContext.getCurrentInstance().addMessage("createForm:inpUsertype", facesMessage);
			FacesContext.getCurrentInstance().addMessage("createForm:inpPhone", facesMessage);
			FacesContext.getCurrentInstance().addMessage("createForm:inpMemo", facesMessage);

			forward = "/createUser.xhtml?faces-redirection-false";
		}
		return forward;		
	}

	public boolean isAdded() {
		return connectedUser !=null;
	}
	//	fin
	public User getById(int id) {		
		connectedUser = userBusiness.getById(connectedUser.getId());
		return connectedUser;		
	}	
	public Long getStorageNbUsers() {
		return userBusiness.getStorageNbUser();
	}
	
	public boolean isfinded() {
		return connectedUser !=null;
	}
			
	public String disconnect() {
		HttpSession session =(HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		connectedUser = null;
		return "/index.xhtml?faces-redirection-true";
	}
	
	public String showPage(String pageId) {
		String forward= null;
		if (pageId.equals("1")) {
			forward = "/storageView.xhtml?faces-redirection=true";
			return forward;
		}else if(pageId.equals("2")) {
			forward = "/productView.xhtml?faces-redirection=true";
			return forward;
		}
		return "/index.xhtml?faces-redirection=true";
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public UserIBusiness getUserBusiness() {
		return userBusiness;
	}
	public void setUserBusiness(UserIBusiness userBusiness) {
		this.userBusiness = userBusiness;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public Integer getPhone() {
		return phone;
	}
    public void setPhone(Integer phone) {
		this.phone = phone;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public List<String> getuTypes() {
		return uTypes;
	}
	public void setuTypes(List<String> uTypes) {
		this.uTypes = uTypes;
	}
	public String getSelectedUType() {
		return selectedUType;
	}
	public void setSelectedUType(String selectedUType) {
		this.selectedUType = selectedUType;
	}
	public User getConnectedUser() {
		return connectedUser;
	}
	public void setConnectedUser(User connectedUser) {
		this.connectedUser = connectedUser;
	}
	public String getPageId() {
		return pageId;
	}
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
}
