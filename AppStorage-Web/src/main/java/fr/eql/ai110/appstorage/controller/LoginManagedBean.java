package fr.eql.ai110.appstorage.controller;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.AccountIBusiness;

@SessionScoped
@ManagedBean(name="mbLogin")
public class LoginManagedBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String login;
	private String pwd;
	private User user;
	
	@EJB
	private AccountIBusiness accountBusiness;
	
	public String connect() {
		String forward = null;
		user = accountBusiness.connect(login,pwd);
		if (isConnected()) {
			forward = "/userView.xhtml?faces-redirection=true";
		}else {
			FacesMessage facesMessage = new FacesMessage(
					FacesMessage.SEVERITY_WARN,
					"Identifiant et/ou mot de passe incorrect(s)",
					"Identifiant et/ou mot de passe incorrect(s)"
					);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpLogin", facesMessage);
			FacesContext.getCurrentInstance().addMessage("loginForm:inpPwd", facesMessage);
			forward = "/login.xhtml?faces-redirection-false";
		}
		return forward;		
	}

	public boolean isConnected() {
		return user !=null;
	}
	
	public String disconnect() {
		HttpSession session =(HttpSession) FacesContext
				.getCurrentInstance()
				.getExternalContext()
				.getSession(true);
		session.invalidate();
		user = null;
		return "/index.xhtml?faces-redirection-true";
	}
	
	public User add() {
		return accountBusiness.add(user);
	}
	
	public String change() {
		String forward = null;
		update();
		if (isUpdated()) {
			forward = "/userView.xhtml?faces-redirection=true";
		}
		return forward;		
	}
	
	public String clearUp() {
		String forward = null;
		delete();
		if (isDeleted()) {
			forward = "/login.xhtml?faces-redirection=true";
		}
		return forward;
	}
	
	public void update() {
		user=accountBusiness.update(user);
	}
	
	public boolean delete() {
		return accountBusiness.delete(user);
	}
	
	public User getById(int id) {
		return accountBusiness.getById(id);
	}
	
	public boolean isUpdated() {
		return user !=null;
	}
	
	public boolean isDeleted() {
		return user == null;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
