package fr.eql.ai110.appstorage.business;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.appstorage.entity.StorageHouse;
import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.StorageHouseIBusiness;
import fr.eql.ai110.appstorage.idao.StorageHouseIDAO;

@Stateless
@Remote(StorageHouseIBusiness.class)
public class StorageHouseBusiness implements StorageHouseIBusiness{
	
	@EJB
	private StorageHouseIDAO storageHouseDAO;

	@Override
	public StorageHouse getById(int id) {
		return storageHouseDAO.getById(id);
	}

	@Override
	public StorageHouse add(StorageHouse sh) {
		return storageHouseDAO.add(sh);
	}

	@Override
	public StorageHouse update(StorageHouse sh) {
		return storageHouseDAO.update(sh);
	}

	@Override
	public List<StorageHouse> getAll() {
		return storageHouseDAO.getAll();
	}

	@Override
	public boolean delete(StorageHouse sh) {
		return storageHouseDAO.delete(sh);
	}

	@Override
	public Set<StorageHouse> getStorageByUser(User user) {
		return storageHouseDAO.getStorageByUser(user);
	}

	@Override
	public StorageHouse getByName(String name) {
		return storageHouseDAO.getByName(name);
	}

}
