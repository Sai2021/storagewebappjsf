package fr.eql.ai110.appstorage.business;

import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.appstorage.entity.Product;
import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.ProductIBusiness;
import fr.eql.ai110.appstorage.idao.ProductIDAO;

@Stateless
@Remote(ProductIBusiness.class)
public class ProductBusiness implements ProductIBusiness{

	@EJB
	private ProductIDAO productDAO;
	
	@Override
	public Product add(Product product) {
		return productDAO.add(product);
	}

	@Override
	public Product update(Product product) {
		return productDAO.update(product);
	}

	@Override
	public boolean delete(Product product) {
		return productDAO.delete(product);
	}

	@Override
	public Product getById(int id) {
		return productDAO.getById(id);
	}

	@Override
	public List<Product> getAll() {
		return productDAO.getAll();
	}

	@Override
	public Set<Product> getProductByUser(User user) {
		return productDAO.getProductByUser(user);
	}

	@Override
	public Product getByName(String name) {
		return productDAO.getByName(name);
	}

}
