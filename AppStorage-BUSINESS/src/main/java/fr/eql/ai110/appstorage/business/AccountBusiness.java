package fr.eql.ai110.appstorage.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.AccountIBusiness;
import fr.eql.ai110.appstorage.idao.UserIDAO;

@Stateless
@Remote(AccountIBusiness.class)
public class AccountBusiness implements AccountIBusiness{

	@EJB
	private UserIDAO userDAO;
	
	@Override
	public User connect(String login, String pwd) {
		return userDAO.authenticate(login, pwd);
	}

	@Override
	public User getById(int id) {
		return userDAO.getById(id);
	}

	@Override
	public User add(User user) {
		return userDAO.add(user);
	}

	@Override
	public User update(User user) {
		return userDAO.update(user);
	}

	@Override
	public boolean delete(User user) {
		return userDAO.delete(user);
	}

}
