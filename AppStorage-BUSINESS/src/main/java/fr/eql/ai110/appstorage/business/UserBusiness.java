package fr.eql.ai110.appstorage.business;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.ibusiness.UserIBusiness;
import fr.eql.ai110.appstorage.idao.UserIDAO;

@Stateless
@Remote(UserIBusiness.class)
public class UserBusiness implements UserIBusiness{
	
	@EJB
	private UserIDAO userDAO;

	//Test parts
	@Override
	public User addPart(String login, String pwd, String usertype, Integer phone, String memo) {
		return userDAO.addPart(login, pwd, usertype, phone, memo);
	}
	//Fin Test parts
	
	@Override
	public User add(User user) {
		return userDAO.add(user);
	}

	@Override
	public User update(User user) {
		return userDAO.update(user);
	}

	@Override
	public boolean delete(User user) {
		return userDAO.delete(user);
	}

	@Override
	public User getById(int id) {
		return userDAO.getById(id);
	}

	@Override
	public Long getStorageNbUser() {
		return userDAO.getStorageNbUser();
	}


}
