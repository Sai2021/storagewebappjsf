package fr.eql.ai110.appstorage.idao;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.appstorage.entity.Product;
import fr.eql.ai110.appstorage.entity.User;

public interface ProductIDAO extends GenericIDAO<Product>{
	Product add(Product product);
	Product update(Product product);
	boolean delete(Product product);
	Product getById(int id);
	List<Product> getAll();
	
	Product getByName(String name);
	Set<Product> getProductByUser(User user);
	

}
