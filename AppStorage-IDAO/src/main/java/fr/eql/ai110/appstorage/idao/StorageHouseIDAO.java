package fr.eql.ai110.appstorage.idao;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.appstorage.entity.StorageHouse;
import fr.eql.ai110.appstorage.entity.User;

public interface StorageHouseIDAO extends GenericIDAO<StorageHouse>{
	
	StorageHouse add(StorageHouse sh);
	StorageHouse update(StorageHouse sh);
	boolean delete(StorageHouse sh);
	StorageHouse getById(int id);
	List<StorageHouse> getAll();
	
	StorageHouse getByName(String name);
	Set<StorageHouse> getStorageByUser(User user);
}
