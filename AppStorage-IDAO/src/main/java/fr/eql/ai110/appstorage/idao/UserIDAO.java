package fr.eql.ai110.appstorage.idao;

import fr.eql.ai110.appstorage.entity.User;

public interface UserIDAO extends GenericIDAO<User>{
	
	User authenticate(String login, String pwd);
	User getById(int id);
	User add(User user);
	User update(User user);
	boolean delete(User user);
	
	
	User addPart(String login, String pwd, String usertype, Integer phone, String memo);
	Long getStorageNbUser();
}
