INSERT INTO `user` (contact, login, memo, phone, pwd, registration_date, usertype) VALUES ('toto@yahoo.fr','toto','blablabla', '123456', 'toto', CURDATE(), 'Client');
INSERT INTO `user` (contact, login, memo, phone, pwd, registration_date, usertype) VALUES ('titi@sfr.fr','titi','blablabla', '123456', 'titi', CURDATE(), 'Partenaire');
INSERT INTO `user` (contact, login, memo, phone, pwd, registration_date, usertype) VALUES ('tutu@oranf.fr','tutu','blablabla', '123456', 'tutu', CURDATE(), 'Fournisseur');
INSERT INTO `user` (contact, login, memo, phone, pwd, registration_date, usertype) VALUES ('tata@gmail.com','tata','blablabla', '123456', 'tata', CURDATE(), 'Fournisseur');
INSERT INTO `user` (contact, login, memo, phone, pwd, registration_date, usertype) VALUES ('lolo@protonmail.com','lolo','blablabla', '123456', 'lolo', CURDATE(), 'Partenaire');

INSERT INTO `storagehouse` (name, address, respuser,create_date,placeall, placeused, user_id) VALUES ('Amazon', '15 avenue Amazon, Rouen', 'Ronan Ronan',CURDATE(), 100,20,1);
INSERT INTO `storagehouse` (name, address, respuser,create_date,placeall, placeused, user_id) VALUES ('Storage1', '1 avenue de Storage, Paris', 'Paris Paris',CURDATE(),60,50,2);
INSERT INTO `storagehouse` (name, address, respuser,create_date,placeall, placeused, user_id) VALUES ('Storage2', '2 avenue de Storage, Lille', 'Lilly Lilly',CURDATE(),50,25,3);
INSERT INTO `storagehouse` (name, address, respuser,create_date,placeall, placeused, user_id) VALUES ('Storage3', '3 avenue de Storage, Lyon', 'Leon Leon',CURDATE(),200,80,4);
INSERT INTO `storagehouse` (name, address, respuser,create_date,placeall, placeused, user_id) VALUES ('Storage4', '4 avenue de Storage, Nice', 'Nike Nike',CURDATE(),120,100,5);

INSERT INTO `product` (address, name,  price, quantity,booking_date,user_id,storagehouse_id) VALUES ('5 Rue de Paris','Four',500, 20, CURDATE(),1, null);
INSERT INTO `product` (address, name,  price, quantity,booking_date,user_id,storagehouse_id) VALUES ('5 Rue de Paris','Four',500, 20, CURDATE(),2, 2);
INSERT INTO `product` (address, name,  price, quantity,booking_date,user_id,storagehouse_id) VALUES ('5 Rue de Paris','Four',500, 20, CURDATE(),null, null);
INSERT INTO `product` (address, name,  price, quantity,booking_date,user_id,storagehouse_id) VALUES ('5 Rue de Paris','Four',500, 20, CURDATE(),3, 3);