package fr.eql.ai110.appstorage.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.appstorage.entity.StorageHouse;
import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.idao.StorageHouseIDAO;

@Stateless
@Remote(StorageHouseIDAO.class)
public class StorageHouseDAO extends GenericDAO<StorageHouse> implements StorageHouseIDAO{

	@Override
	public Set<StorageHouse> getStorageByUser(User user) {
		// TODO Auto-generated method stub
		Query query = em.createQuery("SELECT s FROM StorageHouse s WHERE s.user = :userParam");
		query.setParameter("userParam", user);
		return new HashSet<StorageHouse>(query.getResultList());
	}

	@Override
	public StorageHouse getByName(String name) {
		StorageHouse sh = null;
		List<StorageHouse> shs; 
		Query query = em.createQuery("SELECT s FROM StorageHouse s WHERE s.name = :nameParam");
		query.setParameter("nameParam", name);
		shs = query.getResultList();
		if(shs.size()>0) {
				sh = shs.get(0);
		}
		return sh;
	}
}
