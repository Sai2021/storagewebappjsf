package fr.eql.ai110.appstorage.dao;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.idao.UserIDAO;

@Stateless
@Remote(UserIDAO.class)
public class UserDAO extends GenericDAO<User> implements UserIDAO{
	
	
	@SuppressWarnings("unchecked")
	@Override
	public User authenticate(String login, String pwd) {
		User user = null;
		List<User> users;
		Query query = em.createQuery("SELECT u FROM User u WHERE u.login = :loginParam "
				+ " AND u.pwd = :pwdParam");
		query.setParameter("loginParam", login);
		query.setParameter("pwdParam", pwd);
		users = query.getResultList();
		if(users.size()>0) {
				user = users.get(0);
		}
		return user;
	}
	
	
	@Override
	public User addPart(String login, String pwd, String usertype, Integer phone, String memo) {
		User user = new User();		
		try {
			user.setLogin(login);
			user.setPwd(pwd);
			user.setUsertype(usertype);
			user.setPhone(phone);
			user.setMemo(memo);
			em.persist(user);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return user;
	}


	@Override
	public Long getStorageNbUser() {
		Query query = em.createQuery("SELECT COUNT(u) FROM User u");
		return (Long) query.getSingleResult();
	}

}
