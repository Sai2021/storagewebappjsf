package fr.eql.ai110.appstorage.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.Query;

import fr.eql.ai110.appstorage.entity.Product;
import fr.eql.ai110.appstorage.entity.User;
import fr.eql.ai110.appstorage.idao.ProductIDAO;

@Stateless
@Remote(ProductIDAO.class)
public class ProductDAO extends GenericDAO<Product> implements ProductIDAO{

	@SuppressWarnings("unchecked")
	@Override
	public Set<Product> getProductByUser(User user) {
		Query query = em.createQuery("SELECT p FROM Product p WHERE p.user = :userParam");
		query.setParameter("userParam", user);
		return new HashSet<Product>(query.getResultList());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Product getByName(String name) {
		Product product = null;
		List<Product> products;
		Query query = em.createQuery("SELECT p FROM Product p WHERE p.name = :nameParam");
		query.setParameter("nameParam", name);
		products=query.getResultList();
		if (products.size()>0) {
			product = products.get(0);
		}
		return product;
	}

}
