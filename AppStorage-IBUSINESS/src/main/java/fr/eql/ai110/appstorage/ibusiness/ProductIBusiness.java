package fr.eql.ai110.appstorage.ibusiness;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.appstorage.entity.Product;
import fr.eql.ai110.appstorage.entity.User;

public interface ProductIBusiness {
	Product add(Product product);
	Product update(Product product);
	boolean delete(Product product);
	Product getById(int id);
	List<Product> getAll();
	
	Set<Product> getProductByUser(User user);
	Product getByName(String name);
}
