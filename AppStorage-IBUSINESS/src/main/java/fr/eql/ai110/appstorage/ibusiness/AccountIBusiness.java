package fr.eql.ai110.appstorage.ibusiness;

import fr.eql.ai110.appstorage.entity.User;

public interface AccountIBusiness {
	
	public User connect(String login, String pwd); 
	
	User getById(int id);
	User add(User user);
	User update(User user);
	boolean delete(User user);

}
