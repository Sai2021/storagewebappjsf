package fr.eql.ai110.appstorage.ibusiness;

import java.util.List;
import java.util.Set;

import fr.eql.ai110.appstorage.entity.StorageHouse;
import fr.eql.ai110.appstorage.entity.User;

public interface StorageHouseIBusiness {
	StorageHouse getById(int id);
	StorageHouse add(StorageHouse sh);
	StorageHouse update(StorageHouse sh);
	List<StorageHouse> getAll();
	boolean delete(StorageHouse sh);
	
	StorageHouse getByName(String name);
	Set<StorageHouse> getStorageByUser(User user);
}
