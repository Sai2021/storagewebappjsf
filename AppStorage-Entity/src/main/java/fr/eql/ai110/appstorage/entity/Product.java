package fr.eql.ai110.appstorage.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="address")
	private String address;
	@Column(name="style")
	private String style;
	@Column(name="quantity")
	private Integer quantity;
	@Column(name="price")
	private float price;
	@Column(name="lowlimit")
	private Integer lowlimit;
	@Column(name="highlimit")
	private Integer highlimit;
	@Column(name="validlimit")
	private Integer validlimit;
	@Column(name="alarmdays_date")
	private Date alarmdays;
	@Column(name="booking_date")
	private Date bookingdate;
	@Column(name="bookingcancel_date")
	private Date bookingcanceldate;
	@Column(name="takein_date")
	private Date takeoutdate;
	@Column(name="takeout_date")
	private Date takeindate;
	@Column(name="makedate")
	private Date makedate;
	@Column(name="madein")
	private String madein;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private StorageHouse storagehouse;
	
	

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Integer getLowlimit() {
		return lowlimit;
	}
	public void setLowlimit(Integer lowlimit) {
		this.lowlimit = lowlimit;
	}
	public Integer getHighlimit() {
		return highlimit;
	}
	public void setHighlimit(Integer highlimit) {
		this.highlimit = highlimit;
	}
	public Integer getValidlimit() {
		return validlimit;
	}
	public void setValidlimit(Integer validlimit) {
		this.validlimit = validlimit;
	}
	public Date getAlarmdays() {
		return alarmdays;
	}
	public void setAlarmdays(Date alarmdays) {
		this.alarmdays = alarmdays;
	}
	public Date getBookingdate() {
		return bookingdate;
	}
	public void setBookingdate(Date bookingdate) {
		this.bookingdate = bookingdate;
	}
	public Date getBookingcanceldate() {
		return bookingcanceldate;
	}
	public void setBookingcanceldate(Date bookingcanceldate) {
		this.bookingcanceldate = bookingcanceldate;
	}
	public Date getTakeoutdate() {
		return takeoutdate;
	}
	public void setTakeoutdate(Date takeoutdate) {
		this.takeoutdate = takeoutdate;
	}
	public Date getTakeindate() {
		return takeindate;
	}
	public void setTakeindate(Date takeindate) {
		this.takeindate = takeindate;
	}
	public Date getMakedate() {
		return makedate;
	}
	public void setMakedate(Date makedate) {
		this.makedate = makedate;
	}
	public String getMadein() {
		return madein;
	}
	public void setMadein(String madein) {
		this.madein = madein;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}	
	public StorageHouse getStoragehouse() {
		return storagehouse;
	}
	public void setStoragehouse(StorageHouse storagehouse) {
		this.storagehouse = storagehouse;
	}
		
	public Product(Integer id, String name, String address, String style, Integer quantity, float price,
			Integer lowlimit, Integer highlimit, Integer validlimit, Date alarmdays, Date bookingdate,
			Date bookingcanceldate, Date takeoutdate, Date takeindate, Date makedate, String madein, User user,
			StorageHouse storagehouse) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.style = style;
		this.quantity = quantity;
		this.price = price;
		this.lowlimit = lowlimit;
		this.highlimit = highlimit;
		this.validlimit = validlimit;
		this.alarmdays = alarmdays;
		this.bookingdate = bookingdate;
		this.bookingcanceldate = bookingcanceldate;
		this.takeoutdate = takeoutdate;
		this.takeindate = takeindate;
		this.makedate = makedate;
		this.madein = madein;
		this.user = user;
		this.storagehouse = storagehouse;
	}
	public Product(Integer id, String name, String address, String style, Integer quantity, float price, User user,
			StorageHouse storagehouse) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.style = style;
		this.quantity = quantity;
		this.price = price;
		this.user = user;
		this.storagehouse = storagehouse;
	}
	public Product(Integer id, String name, String address, Integer quantity, float price) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.quantity = quantity;
		this.price = price;
	}
	public Product(Integer id, String name, String address, Integer quantity, float price, 
			Date bookingdate,Date bookingcanceldate, Date takeoutdate, Date takeindate) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.quantity = quantity;
		this.price = price;
		this.bookingdate = bookingdate;
		this.bookingcanceldate = bookingcanceldate;
		this.takeoutdate = takeoutdate;
		this.takeindate = takeindate;
	}
	public Product(Integer id, String name, String address, Integer quantity, float price, 
			Date bookingdate,Date bookingcanceldate, Date takeoutdate, Date takeindate, User user,
				   StorageHouse storagehouse) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.quantity = quantity;
		this.price = price;
		this.bookingdate = bookingdate;
		this.bookingcanceldate = bookingcanceldate;
		this.takeoutdate = takeoutdate;
		this.takeindate = takeindate;
		this.user = user;
		this.storagehouse = storagehouse;
	}
	public Product() {
		super();
	}
	

}
