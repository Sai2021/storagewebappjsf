package fr.eql.ai110.appstorage.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="storagehouse")
public class StorageHouse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="name")
	private String name;
	@Column(name="memo")
	private String memo;
	@Column(name="address")
	private String address;
	@Column(name="create_date")
	private Date createdate;
	@Column(name="delete_date")
	private Date deletedate;
	@Column(name="respuser")
	private String respuser;
	@Column(name="placeall")
	private int placeall;
	@Column(name="placeused")
	private int placeused;
	@Column(name="registration_date")
	private Date registrationdate;
	@Column(name="deregistration_date")
	private Date deregistrationdate;
	@Column(name="validation_date")
	private Date validationdate;
	@ManyToOne
	@JoinColumn(referencedColumnName = "id")
	private User user;
	@OneToMany(mappedBy = "storagehouse", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Product> products;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public Date getDeletedate() {
		return deletedate;
	}
	public void setDeletedate(Date deletedate) {
		this.deletedate = deletedate;
	}
	public String getRespuser() {
		return respuser;
	}
	public void setRespuser(String respuser) {
		this.respuser = respuser;
	}	
	public int getPlaceall() {
		return placeall;
	}
	public void setPlaceall(int placeall) {
		this.placeall = placeall;
	}
	public int getPlaceused() {
		return placeused;
	}
	public void setPlaceused(int placeused) {
		this.placeused = placeused;
	}
	public Date getRegistrationdate() {
		return registrationdate;
	}
	public void setRegistrationdate(Date registrationdate) {
		this.registrationdate = registrationdate;
	}
	public Date getDeregistrationdate() {
		return deregistrationdate;
	}
	public void setDeregistrationdate(Date deregistrationdate) {
		this.deregistrationdate = deregistrationdate;
	}
	public Date getValidationdate() {
		return validationdate;
	}
	public void setValidationdate(Date validationdate) {
		this.validationdate = validationdate;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Set<Product> getProducts() {
		return products;
	}
	public void setProducts(Set<Product> products) {
		this.products = products;
	}	
	public StorageHouse(Integer id, String name, String memo, String address, Date createdate, Date deletedate,
			String respuser, int placeall, int placeused, Date registrationdate, Date deregistrationdate,
			Date validationdate, User user, Set<Product> products) {
		super();
		this.id = id;
		this.name = name;
		this.memo = memo;
		this.address = address;
		this.createdate = createdate;
		this.deletedate = deletedate;
		this.respuser = respuser;
		this.placeall = placeall;
		this.placeused = placeused;
		this.registrationdate = registrationdate;
		this.deregistrationdate = deregistrationdate;
		this.validationdate = validationdate;
		this.user = user;
		this.products = products;
	}
	public StorageHouse(Integer id, String name, String memo, String address, Date createdate, Date deletedate,
			String respuser, int placeall, int placeused, Date registrationdate, Date deregistrationdate, User user,
			Set<Product> products) {
		super();
		this.id = id;
		this.name = name;
		this.memo = memo;
		this.address = address;
		this.createdate = createdate;
		this.deletedate = deletedate;
		this.respuser = respuser;
		this.placeall = placeall;
		this.placeused = placeused;
		this.registrationdate = registrationdate;
		this.deregistrationdate = deregistrationdate;
		this.user = user;
		this.products = products;
	}
	
	public StorageHouse(Integer id, String name, String address, String respuser, User user,
			Set<Product> products) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.respuser = respuser;
		this.user = user;
		this.products = products;
	}
	
	
	public StorageHouse(Integer id, String name, String memo, String address, String respuser, int placeall,
			Date registrationdate, User user, Set<Product> products) {
		super();
		this.id = id;
		this.name = name;
		this.memo = memo;
		this.address = address;
		this.respuser = respuser;
		this.placeall = placeall;
		this.registrationdate = registrationdate;
		this.user = user;
		this.products = products;
	}
	public StorageHouse(Integer id, String name, String memo, String address, Date createdate, Date deletedate,
			String respuser, Date registrationdate, Date deregistrationdate, Date validationdate) {
		super();
		this.id = id;
		this.name = name;
		this.memo = memo;
		this.address = address;
		this.createdate = createdate;
		this.deletedate = deletedate;
		this.respuser = respuser;
		this.registrationdate = registrationdate;
		this.deregistrationdate = deregistrationdate;
		this.validationdate = validationdate;
	}
	public StorageHouse() {
		super();
	}



}
