package fr.eql.ai110.appstorage.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="user")
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Column(name="login")
	private String login;
	@Column(name="pwd")
	private String pwd;
	@Column(name="usertype")
	private String usertype;
	@Column(name="contact")
	private String contact;
	@Column(name="phone")
	private Integer phone;
	@Column(name="memo")
	private String memo;
	@Column(name="registration_date")
	private Date registrationdate;
	@Column(name="deregistration_date")
	private Date deregistrationdate;
	@Column(name="validation_date")
	private Date validationdate;
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<StorageHouse> storagehouses;
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Product> products;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public Integer getPhone() {
		return phone;
	}
	public void setPhone(Integer phone) {
		this.phone = phone;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public Date getRegistrationdate() {
		return registrationdate;
	}
	public void setRegistrationdate(Date registrationdate) {
		this.registrationdate = registrationdate;
	}
	public Date getDeregistrationdate() {
		return deregistrationdate;
	}
	public void setDeregistrationdate(Date deregistrationdate) {
		this.deregistrationdate = deregistrationdate;
	}
	public Date getValidationdate() {
		return validationdate;
	}
	public void setValidationdate(Date validationdate) {
		this.validationdate = validationdate;
	}
	
	public Set<StorageHouse> getStoragehouses() {
		return storagehouses;
	}
	public void setStoragehouses(Set<StorageHouse> storagehouses) {
		this.storagehouses = storagehouses;
	}
	public Set<Product> getProducts() {
		return products;
	}
	public void setProducts(Set<Product> products) {
		this.products = products;
	}
	

	public User(Integer id, String login, String pwd, String usertype, String contact, Integer phone, String memo,
			Date registrationdate, Date deregistrationdate, Date validationdate, Set<StorageHouse> storagehouses,
			Set<Product> products) {
		super();
		this.id = id;
		this.login = login;
		this.pwd = pwd;
		this.usertype = usertype;
		this.contact = contact;
		this.phone = phone;
		this.memo = memo;
		this.registrationdate = registrationdate;
		this.deregistrationdate = deregistrationdate;
		this.validationdate = validationdate;
		this.storagehouses = storagehouses;
		this.products = products;
	}
	
	public User(Integer id, String login, String pwd, String usertype, String contact, Integer phone, String memo,
			Date registrationdate, Date deregistrationdate, Set<StorageHouse> storagehouses,
			Set<Product> products) {
		super();
		this.id = id;
		this.login = login;
		this.pwd = pwd;
		this.usertype = usertype;
		this.contact = contact;
		this.phone = phone;
		this.memo = memo;
		this.registrationdate = registrationdate;
		this.deregistrationdate = deregistrationdate;
		this.storagehouses = storagehouses;
		this.products = products;
	}
	
	public User(Integer id, String login, String pwd, String usertype, String contact, Integer phone, String memo,
			Date registrationdate, Date deregistrationdate, Date validationdate) {
		super();
		this.id = id;
		this.login = login;
		this.pwd = pwd;
		this.usertype = usertype;
		this.contact = contact;
		this.phone = phone;
		this.memo = memo;
		this.registrationdate = registrationdate;
		this.deregistrationdate = deregistrationdate;
		this.validationdate = validationdate;
	}
	public User(Integer id, String login, String pwd, String usertype, Integer phone, String memo
			) {
		super();
		this.id = id;
		this.login = login;
		this.pwd = pwd;
		this.usertype = usertype;
		this.phone = phone;
		this.memo = memo;
	}
	public User(Integer id, String login, String pwd, String usertype, Set<StorageHouse> storagehouses,
			Set<Product> products) {
		super();
		this.id = id;
		this.login = login;
		this.pwd = pwd;
		this.usertype = usertype;
		this.storagehouses = storagehouses;
		this.products = products;
	}
	
	public User(Integer id, String login, String pwd) {
		super();
		this.id = id;
		this.login = login;
		this.pwd = pwd;
	}
	public User() {
		super();
	}

	
	
}
